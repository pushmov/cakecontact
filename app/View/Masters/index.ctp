<h2><?php echo __('Master List'); ?></h2>
<div class="padded-vertical text-right">
	<button class="btn btn-success btn-action" id="download_doc"><i class="fa fa-cloud-download"></i>Export Data</button>
	<button class="btn btn-primary btn-action" id="upload_doc"><i class="fa fa-cloud-upload"></i>Import Data</button>
</div>
<div class="padded-vertical">
	<fieldset>
		<form class="form-horizontal">
			<div class="form-group">
				<label class="control-label">Filter By User:</label>
                <div class="">
					<?php echo $this->Form->input('user', array(
						'options' => $dtUser,
						'class' => 'selectpicker',
						'empty' => 'All',
						'id' => 'user_filter',
						'label' => false
					));?>
                </div>
            </div>
			<div class="form-group">
				<label class="col-sm-4">From Date:</label>
				<div class="col-sm-8">
					<input type="text" name="date" id="date_from" class="form-control datepicker">
				</div>
			</div>
		</form>
	</fieldset>
</div>
<?php echo $this->Session->flash(); ?>
<div class="padded-vertical">
	<table id="dt_master_lists" class="table table-responsive">
        <thead>
            <tr>
                <th>Email</th>
                <th>Name</th>
				<th>Brand</th>
                <th>Source</th>
				<th>Website</th>
                <th>Date Uploaded</th>
                <th>User</th>
            </tr>
        </thead>
    </table>
</div>

<div class="modal fade" id="modal_page" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"></div>