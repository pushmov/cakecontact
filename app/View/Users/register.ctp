<h3><?php echo __('Register new account'); ?></h3>
<fieldset>
	<?php echo $this->Form->create('User', array('class' => 'form-horizontal', 'url' => '/users/doRegister.json')); ?>
	<div class="form-group">
		<label for="username" class="control-label col-sm-2">Username <span class="astrix">*</span></label>
		<div class="col-sm-10">
			<?php echo $this->Form->input('username', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Your username', 'id' => 'username')); ?>
		</div>
	</div>
	
	<div class="form-group">
		<label for="email" class="control-label col-sm-2">Email <span class="astrix">*</span></label>
		<div class="col-sm-10">
			<?php echo $this->Form->input('email', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Your Email', 'id' => 'email'));?>
		</div>
	</div>
	
	<div class="form-group">
		<label for="password" class="control-label col-sm-2">Password <span class="astrix">*</span></label>
		<div class="col-sm-10">
			<?php echo $this->Form->input('password', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Your Password', 'id' => 'password'));?>
		</div>
	</div>
	
	<div class="form-group">
		<label for="confirm_password" class="control-label col-sm-2">Confirm Password</label>
		<div class="col-sm-10">
			<?php echo $this->Form->input('confirm_password', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Confirm Your Password', 'type' => 'password', 'id' => 'confirm_password'));?>
		</div>
	</div>
	
	<div class="form-group">
		<label for="first_name" class="control-label col-sm-2">First Name</label>
		<div class="col-sm-10">
			<?php echo $this->Form->input('first_name', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Your First Name', 'id' => 'first_name'));?>
		</div>
	</div>
	
	<div class="form-group">
		<label for="last_name" class="control-label col-sm-2">Last Name</label>
		<div class="col-sm-10">
			<?php echo $this->Form->input('last_name', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Your Last Name', 'id' => 'last_name'));?>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="button" id="btn_register" class="btn btn-primary"><i class="fa fa-user"></i>Register</button>
			<button type="button" onclick="window.location='/login'" class="btn btn-success"><i class="fa fa-sign-in"></i>Login</button>
		</div>
	</div>
	
	<?php echo $this->Form->end(); ?>
</fieldset>
