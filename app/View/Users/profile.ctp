<h2><?php echo __('Edit Profile'); ?></h2>
<fieldset>
	<?php echo $this->Session->flash('auth');?>
	<?php echo $this->Form->create('User', array('class' => 'form-horizontal', 'url' => '/users/doUpdate.json')); ?>
	<div class="form-group">
		<label for="username" class="control-label col-sm-2">Username <span class="astrix">*</span></label>
		<div class="col-sm-10">
			<?php echo $this->Form->input('username', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Username', 'id' => 'username')); ?>
		</div>
	</div>
	
	<div class="form-group">
		<label for="email" class="control-label col-sm-2">Email <span class="astrix">*</span></label>
		<div class="col-sm-10">
			<?php echo $this->Form->input('email', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Email', 'id' => 'username')); ?>
		</div>
	</div>
	
	<div class="form-group">
		<label for="first_name" class="control-label col-sm-2">First Name</label>
		<div class="col-sm-10">
			<?php echo $this->Form->input('first_name', array('class' => 'form-control', 'label' => false, 'placeholder' => 'First Name', 'id' => 'first_name')); ?>
		</div>
	</div>
	
	<div class="form-group">
		<label for="last_name" class="control-label col-sm-2">Last Name</label>
		<div class="col-sm-10">
			<?php echo $this->Form->input('last_name', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Last Name', 'id' => 'last_name')); ?>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="button" id="back" onclick="window.location='/dashboards'" class="btn btn-success"><i class="fa fa-chevron-circle-left fa-lg"></i>Back</button>
			<button type="button" id="save_profile" class="btn btn-primary"><i class="fa fa-edit fa-lg"></i>Save</button>
			<button type="button" id="change_password" class="btn btn-warning"><i class="fa fa-key fa-lg"></i>Change My Password</button>
		</div>
	</div>
	<?php echo $this->Form->end(); ?>
</fieldset>

<div class="modal fade" id="change_password_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"></div>