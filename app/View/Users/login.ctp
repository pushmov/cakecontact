<h2><?php echo __('Login'); ?></h2>
<fieldset>
	<?php echo $this->Session->flash();?>
	<?php echo $this->Form->create('User', array('class' => 'form-horizontal', 'url' => '/users/doLogin.json')); ?>
	<div class="form-group">
		<label for="UserUsername" class="control-label col-sm-2">Username</label>
		<div class="col-sm-10">
			<?php echo $this->Form->input('username', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Username')); ?>
		</div>
	</div>
	
	<div class="form-group">
		<label for="UserUsername" class="control-label col-sm-2">Password</label>
		<div class="col-sm-10">
			<?php echo $this->Form->input('password', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Password')); ?>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="button" class="btn btn-success btn-action" onclick="window.location='/register'"><i class="fa fa-user fa-lg"></i>Register</button>
			<button type="button" id="btn_login" class="btn btn-primary btn-action"><i class="fa fa-sign-in fa-lg"></i>Login</button>
		</div>
	</div>
	<?php echo $this->Form->end(); ?>
</fieldset>