<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="changePasswordLabel">Remove User</h4>
		</div>
		<div class="modal-body">
			<p>This action will remove user permanently. Continue?</p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>Close</button>
			<button type="button" class="btn btn-danger btn-remove-user" data-id="<?php echo $id; ?>"><i class="fa fa-trash"></i>Continue</button>
		</div>
	</div>
</div>