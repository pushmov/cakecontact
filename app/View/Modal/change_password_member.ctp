<div class="modal-dialog" role="document">
	<div class="modal-content">
		<fieldset>
			<form action="/members/changePassword.json" method="POST" class="form-horizontal">
				<input type="hidden" name="data[User][id]" value="<?php echo $data['User']['id']; ?>">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="changePasswordLabel">Change <?php echo $data['User']['first_name'].' '.$data['User']['last_name']; ?> Password</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="password" class="col-sm-3 control-label">New Password: <span class="astrix">*</span></label>
						<div class="col-sm-9">
							<input type="password" name="data[User][password]" placeholder="New password" class="form-control" id="password">
						</div>
					</div>
					<div class="form-group">
						<label for="new_password_confirm" class="col-sm-3 control-label">Confirm new password: <span class="astrix">*</span></label>
						<div class="col-sm-9">
							<input type="password" name="data[User][confirm]" placeholder="Confirm new password" class="form-control" id="new_password_confirm">
						</div>
					</div>	
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>Close</button>
					<button type="button" id="update_member_password" class="btn btn-primary"><i class="fa fa-edit"></i>Save changes</button>
				</div>
			</form>
		</fieldset>
	</div>
</div>