<div class="modal-dialog" role="document">
	<div class="modal-content">
		<fieldset>
			<form action="/masters/importDocument.json" method="POST" class="form-horizontal" enctype="multipart/form-data" id="import_dialog">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="changePasswordLabel">Import Document</h4>
				</div>
				<div class="modal-body">

					<div class="form-group">
						<div class="upload-file" id="upload_file">Please select your document</div>
					</div>
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>Close</button>
					<button type="button" id="update_my_password" class="btn btn-primary"><i class="fa fa-edit"></i>Import Now</button>
				</div>
			</form>
		</fieldset>
	</div>
</div>