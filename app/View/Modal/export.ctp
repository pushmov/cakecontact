<div class="modal-dialog" role="document">
	<div class="modal-content">
		<fieldset>
			<form action="/masters/exportDocument.json" method="POST" class="form-horizontal">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="changePasswordLabel">Export Document</h4>
				</div>
				<div class="modal-body">

					<div class="form-group">
						export modal
					</div>
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>Close</button>
					<button type="button" id="update_my_password" class="btn btn-primary"><i class="fa fa-edit"></i>Export Now</button>
				</div>
			</form>
		</fieldset>
	</div>
</div>