<div class="modal-dialog" role="document">
	<div class="modal-content">
		<fieldset>
			<form action="/users/changePassword.json" method="POST" class="form-horizontal">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="changePasswordLabel">Change My Password</h4>
				</div>
				<div class="modal-body">

					<div class="form-group">
						<label for="old_password" class="col-sm-3 control-label">Old Password: <span class="astrix">*</span></label>
						<div class="col-sm-9">
							<input type="password" name="data[old_p]" placeholder="Old Password" class="form-control" id="old_password">
						</div>
					</div>
					<div class="form-group">
						<label for="new_password" class="col-sm-3 control-label">New Password: <span class="astrix">*</span></label>
						<div class="col-sm-9">
							<input type="password" name="data[new_p]" placeholder="New password" class="form-control" id="new_password">
						</div>
					</div>
					<div class="form-group">
						<label for="new_password_confirm" class="col-sm-3 control-label">Confirm new password: <span class="astrix">*</span></label>
						<div class="col-sm-9">
							<input type="password" name="data[new_p_confirm]" placeholder="Confirm new password" class="form-control" id="new_password_confirm">
						</div>
					</div>	
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>Close</button>
					<button type="button" id="update_my_password" class="btn btn-primary"><i class="fa fa-edit"></i>Save changes</button>
				</div>
			</form>
		</fieldset>
	</div>
</div>