<h2><?php echo __('Member List'); ?></h2>
<div class="padded-vertical text-right">
	<button class="btn btn-primary" onclick="window.location='/members/add'"><i class="fa fa-plus fa-lg"></i>Add New Member</button>
</div>
<?php echo $this->Session->flash(); ?>
<div class="padded-vertical">
	<table id="dt_member_lists" class="table table-responsive">
        <thead>
            <tr>
                <th>First name</th>
                <th>Last name</th>
                <th>Email</th>
				<th>Username</th>
                <th>Created At</th>
                <th>Modified At</th>
				<th>Status</th>
				<th>Action</th>
            </tr>
        </thead>
    </table>
</div>

<div class="modal fade" id="member_password_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"></div>
<div class="modal fade" id="member_remove_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"></div>