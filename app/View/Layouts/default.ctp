<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo $this->fetch('title'); ?></title>
		<?php
			echo $this->fetch('meta');
			echo $this->fetch('css');
			echo $this->fetch('script');
		?>
		
		<!-- Bootstrap & default theme -->
		<link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
		<link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
		
		<link rel="stylesheet" href="/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css">
		
		<?php if($loadDtTable) : ?>
		<link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
		<link rel="stylesheet" href="/bower_components/datatables.net-responsive-bs/css/responsive.bootstrap.min.css">
		<?php endif; ?>
		
		<?php if($loadDatePicker):?>
		<link rel="stylesheet" href="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css">
		<?php endif; ?>
		
		<?php 
			if($loadUploader):
				echo $this->Html->css('uploadfile'); 
				echo $this->Html->css('uploadfile.custom'); 
			endif;
		?>
		
		<!-- Font awesome -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		
		<!-- Custom styles -->
		<?php echo $this->Html->css('style'); ?>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<?php echo $this->element('header'); ?>
		<section class="ct-container">
			<div class="container">
				<?php echo $this->Flash->render(); ?>
				<?php echo $this->fetch('content'); ?>
			</div>
		</section>
		<?php echo $this->element('footer'); ?>
		<script src="/bower_components/jquery/dist/jquery.min.js"></script>
		<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
		
		<?php if($loadDtTable) : ?>
		<script src="/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
		<script src="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
		<script src="/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
		<script src="/bower_components/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
		<?php endif; ?>
		
		<?php if($loadDatePicker):?>
		<script src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
		<?php endif; ?>
		
		<?php if($loadUploader):?>
		<script src="/js/jquery.uploadfile.min.js"></script>
		<?php endif; ?>
		
		<script src="/js/app.js"></script>
	</body>
</html>