<footer role="contentinfo" class="bs-docs-footer">
	<div class="container">
		<p>Designed and built with all the love in the world by <a target="_blank" href="https://twitter.com/mdo">@mdo</a> and <a target="_blank" href="https://twitter.com/fat">@fat</a>. Maintained by the <a href="https://github.com/orgs/twbs/people">core team</a> with the help of <a href="https://github.com/twbs/bootstrap/graphs/contributors">our contributors</a>.</p> <p>Code licensed <a target="_blank" href="https://github.com/twbs/bootstrap/blob/master/LICENSE" rel="license">MIT</a>, docs <a target="_blank" href="https://creativecommons.org/licenses/by/3.0/" rel="license">CC BY 3.0</a>.
		</p>
	</div>
</footer>