<fieldset>
	<?php echo $this->Form->create('User', array('url' => '/auth/login', 'class' => 'form-horizontal')); ?>
		<div class="form-group">
			<label class="col-sm-2 control-label">Username:</label>
			<div class="col-sm-8">
				<input type="text" class="form-control" placeholder="username" name="data[username]">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Password:</label>
			<div class="col-sm-8">
				<input type="password" class="form-control" placeholder="password" name="data[password]">
			</div>
		</div>
		<div class="form-group text-center">
			<button type="button" class="btn btn-primary" id="btn_login">Login</button>
		</div>
	<?php echo $this->Form->end(); ?>
</fieldset>