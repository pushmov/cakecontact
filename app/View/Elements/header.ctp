<header class="navbar" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/">Contact</a>
		</div>
		<div class="collapse navbar-collapse navbar-right">
                
			<ul class="nav navbar-nav">
			<?php if($this->Session->read('Auth.User.role') == Configure::read('ROLE_ADMIN')):?>
				<li><a href="/members/lists"><i class="fa fa-users"></i>Manage Users</a></li>
			<?php endif; ?>
			<?php if($loggedIn) :?>
				<li>
					<a href="/masters"><i class="fa fa-list"></i>Master List</a>
				</li>
				<li>
					<a href="/users/profile"><i class="fa fa-user"></i>My Profile</a>
				</li>
                <li><a href="/users/logout" id="logout"><i class="fa fa-sign-out"></i>Logout</a></li>
            <?php else : ?>
				<li><a href="/register" id="login"><i class="fa fa-user"></i>Register</a></li>
                <li><a href="/login" id="login"><i class="fa fa-sign-in"></i>Login</a></li>
            <?php endif; ?>
			</ul>
		</div>
	</div>
</header>