<?php

App::uses('AppModel', 'Model');

class Masterlist extends AppModel {
	
	public $belongsTo = array(
		'User' => array(
			'className' => 'User'
		)
	);
	
}