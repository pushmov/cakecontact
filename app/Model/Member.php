<?php

App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class Member extends AppModel {
	public $validate = array(
		'username' => array(
			'required' => array(
				'rule' => 'notBlank',
				'message' => 'Please enter a username'
			),
			'unique' => array(
				'rule' => 'isUnique',
				'message' => 'Username already used'
			)
		),
		'email' => array(
			'required' => array(
				'rule' => 'notBlank',
				'message' => 'Please enter email'
			),
			'unique' => array(
				'rule' => 'isUnique',
				'message' => 'Email already used'
			)
		),
		'password' => array(
			'required' => array(
				'rule' => 'notBlank',
				'message' => 'Please enter password'
			),
			'between' => array(
                'rule' => array('lengthBetween', 8, 15),
                'message' => 'Between 8 to 15 characters'
            )
		),
	);
	
	public function beforeSave($options = array()) {
		if (!parent::beforeSave($options)) {
			return false;
		}
		if (isset($this->data[$this->alias]['password'])) {
			$hasher = new BlowfishPasswordHasher();
			$this->data[$this->alias]['password'] = $hasher->hash($this->data[$this->alias]['password']);
		}
		return true;
	}

}