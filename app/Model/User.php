<?php

App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {
	public $validate = array(
		'username' => array(
			'required' => array(
				'rule' => 'notBlank',
				'message' => 'Please enter a username'
			),
			'unique' => array(
				'rule' => 'isUnique',
				'message' => 'Username already used'
			)
		),
		'password' => array(
			'required' => array(
				'rule' => 'notBlank',
				'message' => 'Please enter password'
			),
			'between' => array(
                'rule' => array('lengthBetween', 5, 15),
                'message' => 'Password length between 5 to 15 characters'
            )
		),
		'email' => array(
			'required' => array(
				'rule' => 'notBlank',
				'message' => 'Please enter email'
			),
			'unique' => array(
				'rule' => 'isUnique',
				'message' => 'Email already used'
			),
			'email' => array(
				'rule' => array('email', true),
				'message' => 'Please supply a valid email address.'
			)
		),
		'confirm_password' => array(
			'identicalFieldValues' => array(
				'rule' => array('identicalFieldValues', 'password'),
				'message' => 'Please re-enter your password twice so that the values match'
			)
		)
	);
	
	public function beforeSave($options = array()) {
		if (!parent::beforeSave($options)) {
			return false;
		}
		if (isset($this->data[$this->alias]['password'])) {
			$hasher = new BlowfishPasswordHasher();
			$this->data[$this->alias]['password'] = $hasher->hash($this->data[$this->alias]['password']);
		}
		return true;
	}
	
	public function isOldPasswordMatch($userId, $oldPassword){
		$user = $this->find('first', array(
			'conditions' => array('User.id' => $userId)
		));
		$newHash = Security::hash($oldPassword, 'blowfish', $user['User']['password']);
		if($newHash == $user['User']['password']) {
			return true;
		}
		return false;
	}
	
	function identicalFieldValues( $field=array(), $compare_field=null ) {
        foreach( $field as $key => $value ){
            $v1 = $value;
            $v2 = $this->data[$this->name][ $compare_field ];                 
            if($v1 !== $v2) {
                return FALSE;
            } else {
                continue;
            }
        }
        return TRUE;
    } 
	

}