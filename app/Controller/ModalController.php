<?php
// app/Controller/UsersController.php
App::uses('AppController', 'Controller');

class ModalController extends AppController {
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->autoLayout = false;
		if( !$this->request->is('ajax') ){
			throw new NotFoundException(__('Invalid request'));
		}
	}
	
	public function changePassword(){
		return $this->render('/Modal/change_password', 'plain', null);
	}
	
	public function changeMemberPassword(){
		$this->loadModel('User');
		$user = $this->User->find('first', array(
			'conditions' => array('User.id' => $this->request->query['id'])
		));
		$this->set('data', $user);
		return $this->render('/Modal/change_password_member', 'plain', null);
	}
	
	public function importDialog(){
		return $this->render('/Modal/import', 'plain', null);
	}
	
	public function confirmRemove(){
		$this->set('id', $this->request->query['id']);
		return $this->render('/Modal/confirm_remove', 'plain', null);
	}
}