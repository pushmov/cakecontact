<?php
// app/Controller/UsersController.php
App::uses('AppController', 'Controller');
App::import('Vendor', 'PHPExcel', array('file' => 'PHPExcel'.DS.'PHPExcel.php'));
App::import('Vendor', 'PHPExcel_IOFactory', array('file' => 'PHPExcel'.DS.'PHPExcel'.DS.'IOFactory.php'));

class MastersController extends AppController {
	
	public function beforeFilter() {
		parent::beforeFilter();
	}
	
	public function doImport($filename='sample.xlsx'){
		$this->loadModel('Masterlist');
		$inputFileName = APP.'webroot/files/'.$filename;
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
			return json_encode(array('errors' => $e->getMessage()));
		}
		//  Get worksheet dimensions
		$sheet = $objPHPExcel->getSheet(0); 
		$highestRow = $sheet->getHighestRow();
		$highestColumn = $sheet->getHighestColumn();
		//  Loop through each row of the worksheet in turn
		for ($row = 1; $row <= $highestRow; $row++){ 
			//  Read a row of data into an array
			$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
				NULL,
				TRUE,
				FALSE);
			//  Insert row data array into your database of choice here
			$brand = $rowData[0][0];
			$website = $rowData[0][1];
			$email = $rowData[0][2];
			$source = $rowData[0][3];
			$conditions = array(
				'Masterlist.email' => $email
			);
			if($row > 1 && $email != '' && !$this->Masterlist->hasAny($conditions)){
				$this->Masterlist->create();
				$name = explode('@', $email);
				$data['Masterlist'] = array(
					'email' => $email,
					'name' => $name[0],
					'brand' => $brand,
					'source' => $source,
					'website' => $website,
					'user_id' => $this->Session->read('Auth.User.id')
				);
				$this->Masterlist->save($data);
			}
		}
		return json_encode(array('success' => true));
	}
	
	public function index(){
		$this->loadModel('User');
		$users = $this->User->find('all', array(
			'fields' => array('User.username', 'User.id')
		));
		$dtUser = array('' => 'All');
		foreach($users as $row){
			$dtUser[$row['User']['id']] = $row['User']['username'];
		}
		$this->set('dtUser', $dtUser);
		$this->set('loadDtTable', true);
		$this->set('loadDatePicker', true);
		$this->set('loadUploader', true);
	}
	
	public function listData(){
		$this->autoRender = false;
		$this->loadModel('Masterlist');
		$conditions = array();
		$orderable = array('Masterlist.email', 'Masterlist.name', 'Masterlist.brand', 'Masterlist.source', 'Masterlist.website', 'Masterlist.date');
		if($this->request->query['search']['value'] != ''){
			foreach($orderable as $val){
				$conditions['OR'][$val .' LIKE'] = '%'.$this->request->query['search']['value'].'%';
			}
		}
		
		//custom user filter select
		if($this->request->query['user'] != ''){
			$conditions['Masterlist.user_id'] = $this->request->query['user'];
		}
		
		$data['draw'] = $this->request->query['draw'];
		$allRecords = $this->Masterlist->find('count', array(
			'conditions' => $conditions
		));
		$masterlist = $this->Masterlist->find('all', array(
			'conditions' => $conditions,
			'order' => array($orderable[$this->request->query['order'][0]['column']].' '.  strtoupper($this->request->query['order'][0]['dir'])),
			'limit' => $this->request->query['length'],
			'offset' => $this->request->query['start']
		));
		$data['data'] = array();
		foreach($masterlist as $master){
			$row = array(
				$master['Masterlist']['email'],
				$master['Masterlist']['name'],
				$master['Masterlist']['brand'],
				$master['Masterlist']['source'],
				'<a href="'.$master['Masterlist']['website'].'" target="_blank">'.$master['Masterlist']['website'].'</a>',
				date_create($master['Masterlist']['date'])->format(Configure::read('DATE_FORMAT')),
				$master['User']['username']
			);
			$data['data'][] = array_values($row);
		}
		$data['recordsTotal'] = $allRecords;
		$data['recordsFiltered'] = $allRecords;
		
		return json_encode($data);
	}
	
	public function doUpload(){
		$this->autoRender = false;
		$allowed_files = array('csv', 'xls', 'xlsx');
		if ($this->request->is('ajax')) {
			$extensions = explode('.', $this->request->data['myfile']['name']);
			if( !in_array(end($extensions), $allowed_files)) {
				return json_encode(array('errors' => __('File is not allowed')));
			}
			$new_file_name = time().'.'.end($extensions);
			if(!is_dir(APP.'webroot/files')){
				mkdir(APP.'webroot/files');
			}
			if(move_uploaded_file($this->request->data['myfile']['tmp_name'], APP.'webroot/files/'.$new_file_name)){
				return $this->doImport($new_file_name);
			} else {
				return json_encode(array('errors' => 'Unknown error. Please try again'));
			}
		}
	}
	
}