<?php
// app/Controller/UsersController.php
App::uses('AppController', 'Controller');

class MembersController extends AppController {
	
	public function beforeFilter() {
		parent::beforeFilter();
		if ($this->Session->read('Auth.User.role') == Configure::read('ROLE_USER')) {
			$this->redirect(array('controller' => 'dashboard', 'action' => 'index'));
		}
	}
	
	public function add() {
		
	}
	
	public function edit($id = null) {
		$this->loadModel('User');
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
        }
		$this->request->data = $this->User->findById($id);
		unset($this->request->data['User']['password']);
		$this->set('id', $id);
	}
	
	public function submitUser() {
		$this->autoRender = false;
		$this->loadModel('User');
		if($this->request->is('ajax')){
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('Success! New member saved'), 'default', array('class' => 'alert alert-success', 'role' => 'alert'));
				return json_encode(array('success' => true, 'url' => '/members/lists'));
			} else {
				return json_encode(array('errors' => $this->User->validationErrors));
			}
		}
	}
	
	public function updateUser() {
		$this->autoRender = false;
		$this->loadModel('User');
		if($this->request->is('ajax')) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('Success! Data updated'), 'default', array('class' => 'alert alert-success', 'role' => 'alert'));
				return json_encode(array('success' => true, 'url' => '/members/lists'));
			} else {
				return json_encode(array('errors' => $this->User->validationErrors));
			}
		}
	}
	
	public function lists() {
		$this->set('loadDtTable', true);
	}
	
	public function loadUsers() {
		$this->autoRender = false;
		$this->loadModel('User');
		$conditions['role'] = Configure::read('ROLE_USER');
		$orderable = array('User.first_name', 'User.last_name', 'User.email', 'User.username', 'User.created', 'User.modified');
		if($this->request->query['search']['value'] != ''){
			foreach($orderable as $val){
				$conditions['OR'][$val .' LIKE'] = '%'.$this->request->query['search']['value'].'%';
			}
		}
		$data['draw'] = $this->request->query['draw'];
		$allRecords = $this->User->find('count', array(
			'conditions' => $conditions
		));
		$users = $this->User->find('all', array(
			'conditions' => $conditions,
			'order' => array($orderable[$this->request->query['order'][0]['column']].' '.  strtoupper($this->request->query['order'][0]['dir'])),
			'limit' => $this->request->query['length'],
			'offset' => $this->request->query['start']
		));
		$data['data'] = array();
		foreach($users as $user){
			$action = '<button class="btn btn-action btn-small btn-primary" onclick="window.location=\'/members/edit/'.$user['User']['id'].'\'"><i class="fa fa-edit"></i>Modify</button>';
			$action .= '<button class="btn btn-action btn-small btn-warning modal-change-password" data-id="'.$user['User']['id'].'"><i class="fa fa-key"></i>Change Password</button>';
			$action .= '<button class="btn btn-action btn-small btn-danger btn-remove-user" data-id="'.$user['User']['id'].'"><i class="fa fa-trash"></i>Delete</button>';
			if($user['User']['is_approved'] == Configure::read('STATUS_PENDING')){
				$action .= '<button class="btn btn-action btn-small btn-success btn-approve-user" data-id="'.$user['User']['id'].'"><i class="fa fa-thumbs-up"></i>Approve</button>';
				$status = '<span class="label label-danger">'.Configure::read('STATUS_TXT')[$user['User']['is_approved']].'</span>';
			} else {
				$status = '<span class="label label-success">'.Configure::read('STATUS_TXT')[$user['User']['is_approved']].'</span>';
			}
			$row = array(
				$user['User']['first_name'],
				$user['User']['last_name'],
				$user['User']['email'],
				$user['User']['username'],
				date_create($user['User']['created'])->format(Configure::read('DATETIME_FORMAT')),
				date_create($user['User']['modified'])->format(Configure::read('DATETIME_FORMAT')),
				$status,
				$action
			);
			$data['data'][] = array_values($row);
		}
		$data['recordsTotal'] = $allRecords;
		$data['recordsFiltered'] = $allRecords;
		
		return json_encode($data);
	}
	
	public function removeUser() {
		$this->autoRender = false;
		$this->loadModel('User');
		if($this->request->is('ajax') && $this->request->is('post')){
			if($this->User->delete($this->request->data['id'])){
				return json_encode(array('success' => true));
			} else {
				return json_encode(array('errors' => $this->User->validationErrors));
			}
		}
	}
	
	public function changePassword() {
		$this->autoRender = false;
		if ($this->request->data['User']['password'] != $this->request->data['User']['confirm']) {
			return json_encode(array('errors' => true, 'message' => __('Password do not match')));
		}
		$this->loadModel('User');
		$this->User->id = $this->request->data['User']['id'];
		unset($this->request->data['User']['confirm']);
		if($this->User->save($this->request->data)){
			return json_encode(array('success' => true));
		} else {
			return json_encode(array('errors' => $this->User->validationErrors));
		}
	}
	
	public function approveUser() {
		$this->autoRender = false;
		$this->loadModel('User');
		if($this->request->is('ajax') && $this->request->is('post')){
			$this->request->data = $this->User->find('first', array(
				'conditions' => array('User.id' => $this->request->data['id'])
			));
			$this->request->data['User']['is_approved'] = Configure::read('STATUS_APPROVED');
			unset($this->request->data['User']['password']);
			if($this->User->save($this->request->data)){
				return json_encode(array('success' => true));
			} else {
				return json_encode(array('errors' => $this->User->validationErrors));
			}
		}
	}
}