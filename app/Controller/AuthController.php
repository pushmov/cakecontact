<?php

App::uses('AppController', 'Controller');

class AuthController extends Controller{
	
	public $components = array('RequestHandler');
	
	public function login(){
		$title = 'Login';
		$this->set('title_for_layout', $title);
	}
	
	public function postLogin(){
		$this->loadModel('User');
		$this->autoRender = false;
		if(!$this->request->is('ajax')){
			throw new BadRequestException();
		}
		$response = $this->User->login($this->request['data']);
		return json_encode($response);
	}
	
	public function test(){
		$this->autoRender = false;
		echo 'a';
	}
	
}