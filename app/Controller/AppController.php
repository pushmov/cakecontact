<?php

App::uses('Controller', 'Controller');

class AppController extends Controller {
	
	public $components = array(
		'Session',
		'Auth' => array(
			'loginRedirect' => array('controller' => 'dashboards', 'action' => 'index'),
			'logoutRedirect' => array(
				'controller' => 'users',
				'action' => 'login'
			),
			'authenticate' => array(
				'Form' => array(
					'passwordHasher' => 'Blowfish'
				)
			)
		)
	);
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('loggedIn', $this->Auth->login());
		$this->set('userSession', $this->Session->read('Auth.User'));
		$this->set('loadDtTable', false);
		$this->set('loadDatePicker', false);
		$this->set('loadUploader', false);
	}
}
