<?php
// app/Controller/UsersController.php
App::uses('AppController', 'Controller');

class DashboardsController extends AppController {
	
	public function beforeFilter() {
		parent::beforeFilter();
		if($this->Session->read('Auth.User.is_approved') == Configure::read('STATUS_PENDING')){
			$this->Session->setFlash(__('Your account not yet approved'), 'default', array('class' => 'alert alert-warning'));
			$this->redirect(array('controller' => 'users', 'action' => 'logout'));
		}
	}
	
	public function index(){
		
		$this->loadModel('Masterlist');
		$test = $this->Masterlist->find('all');
	}
	
}