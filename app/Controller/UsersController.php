<?php
// app/Controller/UsersController.php
App::uses('AppController', 'Controller');

class UsersController extends AppController {

    public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow();
	}

	public function register() {
		
	}
	
	public function doRegister() {
		$this->autoRender = false;
		if ($this->request->is('ajax') && $this->request->is('post')){
			$this->User->create();
			if($this->User->save($this->request->data)){
				return json_encode(array('success' => true));
			} else {
				return json_encode(array('errors' => $this->User->validationErrors));
			}
		}
	}

	public function login() {
		if($this->Auth->login()){
			$this->redirect(array('controller' => 'dashboards', 'action' => 'index'));
		}
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				return $this->redirect($this->Auth->redirectUrl());
			}
			$this->Session->setFlash(__('Incorrect username or password'));
		}
	}
	
	public function doLogin() {
		$this->autoRender = false;
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				return json_encode(array('success' => true, 'url' => $this->Auth->redirectUrl(), 'message' => ''));
			}
			return json_encode(array('message' => __('Incorrect username or password'), 'errors' => true, 'url' => '/dashboards'));
		}
	}

	public function logout() {
		return $this->redirect($this->Auth->logout());
	}
	
	public function profile() {
		$id = $this->Session->read('Auth.User.id');
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
        }
		$this->request->data = $this->User->findById($id);
		unset($this->request->data['User']['password']);
	}
	
	public function doUpdate() {
		$this->autoRender = false;
		$this->User->id = $this->Session->read('Auth.User.id');
		if ($this->User->save($this->request->data)) {
			return json_encode(array('success' => true));
		} else {
			return json_encode(array('errors' => $this->User->validationErrors));
		}
	}
	
	public function changePassword(){
		$this->autoRender = false;
		if($this->request->is('ajax')){
			if($this->request->data['old_p'] == ''){
				return json_encode(array('errors' => array('message' => __('Old Password required'), 'label' => 'old_password')));
			}
			
			if($this->request->data['new_p'] == ''){
				return json_encode(array('errors' => array('message' => __('New Password required'), 'label' => 'new_password')));
			}
			
			if($this->request->data['new_p_confirm'] == ''){
				return json_encode(array('errors' => array('message' => __('New Password confirm required'), 'label' => 'new_password_confirm')));
			}
			
			if($this->request->data['new_p'] != $this->request->data['new_p_confirm']){
				return json_encode(array('errors' => array('message' => __('Password do not match'), 'label' => 'confirm')));
			}
			
			if(!$this->User->isOldPasswordMatch($this->Session->read('Auth.User.id'), $this->request->data['old_p'])){
				return json_encode(array('errors' => array('message' => __('Your password is incorrect'), 'label' => 'old_password')));
			}
			$newPass = $this->request->data['new_p'];
			$this->request->data = $this->User->find('first', array(
				'conditions' => $this->Session->read('Auth.User.id')
			));
			$this->request->data['User']['password'] = $newPass;
			if($this->User->save($this->request->data)){
				return json_encode(array('success' => true));
			} else {
				return json_encode(array('invalid' => $this->User->validationErrors));
			}
			
		}
	}

}