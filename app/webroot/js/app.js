$(document).ready(function(){
    var loader = '<i class="fa fa-spinner fa-spin"></i>Processing...';
    
    $('#btn_login').on('click', function(){
        var btn = $(this);
        var txt = btn.html();
        btn.html(loader);
        btn.prop('disabled', true);
        var form = $(this).closest('form');
        form.find('.alert').remove();
        $.post(form.attr('action'), form.serialize(), function(data){
            btn.html(txt);
            btn.prop('disabled', false);
            var json = $.parseJSON(data);
            if(json.errors){
                form.prepend('<div class="alert alert-danger" role="alert">'+json.message+'</div>');
            } else if(json.success){
                window.location = json.url;
            }
        });
    });
    
    $('#btn_register').on('click', function(){
        var btn = $(this);
        var txt = btn.html();
        btn.html(loader);
        btn.prop('disabled', true);
        var form = $(this).closest('form');
        form.find('.errors').remove();
        $.post(form.attr('action'), form.serialize(), function(data){
           btn.html(txt);
           btn.prop('disabled', false);
           var json = $.parseJSON(data);
           if(json.errors){
               $.each(json.errors, function(k, v){
                   form.find('input#'+k).after('<small class="errors">'+v+'</small>');
               });
           } else if(json.success){
               form.find('input').val('');
               form.prepend('<div class="alert alert-success">Congratulations. Your account was created. Admin will review your account in 2x24 hours max</div>');
           }
        });
    });
    
    $('#UserPassword').on('keypress', function(e){
        if(e.which === 13){
            $('#btn_login').click();
        }
    });
    
    $('#clear_form').on('click', function(){
        var btn = $(this);
        var txt = btn.html();
        btn.html(loader);
        btn.prop('disabled', true);
        $(this).closest('form').find('input').val('');
        btn.prop('disabled', false);
        btn.html(txt);
    });
    
    $('#add_member').on('click', function(){
        var btn = $(this);
        var txt = btn.html();
        btn.html(loader);
        btn.prop('disabled', true);
        var form = $(this).closest('form');
        form.find('.errors').remove();
        $.post(form.attr('action'), form.serialize(), function(data){
            btn.html(txt);
            btn.prop('disabled', false);
            var json = $.parseJSON(data);
            if(json.errors){
                $.each(json.errors, function(k, v){
                   form.find('input[name="data[User][' + k + ']"]').after('<small class="errors">' + v + '</small>'); 
                });
            } else if(json.success){
                window.location = '/members/lists';
            }
        });
    });
    
    if($('#dt_member_lists').length > 0){
        $('#dt_member_lists').DataTable( {
            processing: true,
            serverSide: true,
            ajax: '/members/loadUsers.json',
            responsive: true,
            order: [[4, 'desc']],
            aoColumnDefs: [
                { bSortable: false, aTargets: [ 6 ] }
			]
        });
    }
    
    if($('#dt_master_lists').length > 0){
        var dt = $('#dt_master_lists').DataTable( {
            processing: true,
            serverSide: true,
            ajax: {
                url: '/masters/listData.json',
                data: function(d){
                    d.user = $('#user_filter').val();
                }
            },
            responsive: true,
            order: [[5, 'asc']]
        });
		$('#user_filter').on('change', function(){
			dt.ajax.reload();
		});
    }
    
    $('#save_profile').on('click', function(){
        var btn = $(this);
        var txt = btn.html();
        btn.html(loader);
        btn.prop('disabled', true);
        var form = $(this).closest('form');
        form.find('.errors').remove();
        form.find('.alert').remove();
        $.post(form.attr('action'), form.serialize(), function(data){
            btn.prop('disabled', false);
            btn.html(txt);
            var json = $.parseJSON(data);
            if(json.errors) {
                $.each(json.errors, function(k, v){
                   form.find('input[name="data[User][' + k + ']"]').after('<small class="errors">' + v + '</small>'); 
                });
            } else if(json.success) {
                form.prepend('<div class="alert alert-success">Your profile has been updated</div>');
            }
        });
    });
    
    $('#update_member').on('click', function(){
        var btn = $(this);
        var txt = btn.html();
        btn.prop('disabled', true);
        btn.html(loader);
        var form = $(this).closest('form');
        form.find('.errors').remove();
        form.find('.alert').remove();
        $.post(form.attr('action'), form.serialize(), function(data){
            btn.html(txt);
            btn.prop('disabled', false);
            var json = $.parseJSON(data);
            if(json.errors) {
                $.each(json.errors, function(k, v){
                    form.find('input[name="data[User][' + k + ']"]').after('<small class="errors">' + v + '</small>');
                });
            } else if(json.success) {
                window.location = json.url;
            }
        });
    });
    
    $('#change_password').on('click', function(){
        var btn = $(this);
        var txt = btn.html();
        btn.prop('disabled', true);
        btn.html(loader);
        $.get('/modal/changePassword', function(html){
            btn.prop('disabled', false);
            btn.html(txt);
            $('#change_password_modal').html(html);
            $('#change_password_modal').modal('show');
        });
    });
    
    $('#change_password_modal').on('click', '#update_my_password', function(){
        var btn = $(this);
        var txt = btn.html();
        btn.prop('disabled', true);
        btn.html(loader);
        var form = $(this).closest('form');
        form.find('.errors').remove();
        form.find('.alert').remove();
        $.post(form.attr('action'), form.serialize(), function(data){
            btn.prop('disabled', false);
            btn.html(txt);
            var json = $.parseJSON(data);
            if(json.success) {
                form.find('.modal-body').prepend('<div class="alert alert-success">Success. Your Password has been changed</div>');
                form.find('input').val('');
            } else if(json.errors){
                form.find('input#'+json.errors.label).after('<small class="errors">'+json.errors.message+'</small>');
                if(json.errors.label === 'confirm'){
                    form.find('input#new_password, input#new_password_confirm').after('<small class="errors">'+json.errors.message+'</small>');
                }
            } else if(json.invalid){
                $.each(json.invalid, function(k, v){
                    form.find('input#new_password, input#new_password_confirm').after('<small class="errors">'+v+'</small>');
                });
            }
        });
    });
    
    $(document).on('click', '.modal-change-password', function(){
        var btn = $(this);
        var txt = btn.html();
        btn.prop('disabled', true);
        btn.html(loader);
        $.get('/modal/changeMemberPassword/', {id: $(this).attr('data-id')}, function(html){
            btn.prop('disabled', false);
            btn.html(txt);
            $('#member_password_modal').html(html);
            $('#member_password_modal').modal('show');
        });
    });
    
    $('#member_password_modal').on('click', '#update_member_password', function(){
        var btn = $(this);
        var txt = btn.html();
        btn.html(loader);
        btn.prop('disabled', true);
        var form = $(this).closest('form');
        form.find('.alert').remove();
        form.find('.errors').remove();
        $.post(form.attr('action'), form.serialize(), function(data){
            btn.prop('disabled', false);
            btn.html(txt);
            var json = $.parseJSON(data);
            if(json.success){
                form.find('.modal-body').prepend('<div class="alert alert-success">Success. Password has been changed</div>');
                form.find('input').val('');
            } else if (json.errors){
                if(json.message){
                    form.find('input#new_password_confirm').after('<small class="errors">'+json.message+'</small>');
                } else {
                    $.each(json.errors, function(k, v){
                        form.find('input#'+k).after('<small class="errors">'+v+'</small>');
                    });
                }
            }
        });
    });
    
    if($('.datepicker').length > 0) {
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd'
        });
    }
    
    $('#upload_doc').on('click', function(){
        $.get('/modal/importDialog', function(html){
            $('#modal_page').html(html);
            $('#modal_page').modal('show');
            $('#upload_file').uploadFile({
                url: '/masters/doUpload',
                fileName: 'data[myfile]',
                multiple:false,
                dragDrop:false,
                maxFileCount:1,
                acceptFiles: '.xls, .xlsx, .csv',
                returnType:'json',
                onSuccess:function(files,data,xhr,pd){
                    var dialog = $(document).find('#import_dialog');
                    if(data.errors){
                        dialog.prepend('<div class="alert alert-warning">'+data.errors+'</div>');
                    } else {
                        dialog.find('.modal-footer').html('<button class="btn btn-small btn-success" id="btn_import_complete">Success</button>');
                        dialog.on('click', '#btn_import_complete',function(){
                            window.location = '/masters';
                        });
                    }
                }
            });
        });
    });
    
    
    $(document).on('click', '.btn-approve-user', function(){
        var btn = $(this);
        var txt = btn.html();
        btn.html(loader);
        btn.prop('disabled', true);
        var id = $(this).attr('data-id');
        $.post('/members/approveUser.json', {id: id}, function(data){
            btn.prop('disabled', false);
            btn.html(txt);
            var json = $.parseJSON(data);
            if(json.success){
                window.location.reload();
            }
        });
    });
    
    $(document).on('click', '.btn-remove-user', function(){
        var btn = $(this);
        var txt = btn.html();
        btn.html(loader);
        btn.prop('disabled', true);
        $.get('/modal/confirmRemove', {id: btn.attr('data-id')}, function(html){
            btn.prop('disabled', false);
            btn.html(txt);
            $('#member_remove_modal').html(html);
            $('#member_remove_modal').modal('show');
            $('#member_remove_modal').on('click', '.btn-remove-user', function(){
                var btn = $(this);
                var txt = btn.html();
                btn.html(loader);
                btn.prop('disabled', true);
                $.post('/members/removeUser.json', {id: btn.attr('data-id')}, function(data){
                    btn.prop('disabled', false);
                    btn.html(txt);
                    var json = $.parseJSON(data);
                    if(json.success){
                        window.location = '/members/lists';
                    }
                });
            });
        });
    });
    
});